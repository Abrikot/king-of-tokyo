/**
 * Messages that are sent from a remote application to another.
 */
package se.ltu.softwareengineering.communication.message;