/**
 * Base layer of the communication between two remotely connected application.
 * <br>
 * Depend on {@link se.ltu.softwareengineering.exception} and {@link se.ltu.softwareengineering.tool}
 */
package se.ltu.softwareengineering.communication;