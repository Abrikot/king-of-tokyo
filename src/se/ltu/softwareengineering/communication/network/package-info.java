/**
 * Interface to network operation (server and client).
 * <br>
 * Depend on {@link se.ltu.softwareengineering.communication.message}, {@link se.ltu.softwareengineering.exception}
 * and {@link se.ltu.softwareengineering.tool}
 */
package se.ltu.softwareengineering.communication.network;