/**
 * Main package for this King of Tokyo Power Up! implementation.
 * Contain every other package this project should need.
 */
package se.ltu.softwareengineering;