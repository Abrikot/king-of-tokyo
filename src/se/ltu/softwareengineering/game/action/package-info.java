/**
 * Contain action that a player can do.
 * <br>
 * Examples of actions are dealing damages, gaining stars, hearts, ...
 */
package se.ltu.softwareengineering.game.action;