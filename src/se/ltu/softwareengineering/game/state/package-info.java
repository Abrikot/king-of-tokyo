/**
 * Classes that represent the current state of the game. Contain all the information about the game.
 * <br>
 * Depend on {@link se.ltu.softwareengineering.concept} and {@link se.ltu.softwareengineering.player}
 */
package se.ltu.softwareengineering.game.state;