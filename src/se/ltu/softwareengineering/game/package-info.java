/**
 * Manage the whole game. Link every package.
 * <br>
 * Depend on {@link se.ltu.softwareengineering.communication}, {@link se.ltu.softwareengineering.concept},
 * {@link se.ltu.softwareengineering.exception}, {@link se.ltu.softwareengineering.io},
 * {@link se.ltu.softwareengineering.player} and {@link se.ltu.softwareengineering.tool}
 */
package se.ltu.softwareengineering.game;