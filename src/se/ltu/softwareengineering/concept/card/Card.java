package se.ltu.softwareengineering.concept.card;

import se.ltu.softwareengineering.concept.Concept;
import se.ltu.softwareengineering.concept.EffectProvider;
import se.ltu.softwareengineering.concept.Spell;

import java.util.List;
import java.util.Objects;

public abstract class Card implements Concept, EffectProvider {
    private String name;
    private String description;
    private List<Spell> effects;
    private CardType type;

    Card(String name, String description, CardType type, List<Spell> effects) {
        this.name = name;
        this.description = description;
        this.type = type;
        this.effects = effects;
    }

    @Override
    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public CardType getType() {
        return type;
    }

    @Override
    public List<Spell> getEffects() {
        return effects;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Card card = (Card) o;
        return Objects.equals(getName(), card.getName()) &&
                Objects.equals(getDescription(), card.getDescription()) &&
                Objects.equals(getEffects(), card.getEffects()) &&
                getType() == card.getType();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getDescription(), getEffects(), getType());
    }

    @Override
    public String toString() {
        return "Card{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", effects=" + effects +
                ", type=" + type +
                '}';
    }
}
