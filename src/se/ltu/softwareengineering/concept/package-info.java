/**
 * Package of classes representing key-concepts of the game.
 * <br>
 * Used as default containers for raw information extracted from data files.
 */
package se.ltu.softwareengineering.concept;