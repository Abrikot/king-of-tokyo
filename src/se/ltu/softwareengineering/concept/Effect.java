package se.ltu.softwareengineering.concept;

public enum Effect {
    AlterDamages,
    AlterArmor,
    AlterCardCost,
    GainStars,
    GainEnergy,
    GainHearts,
    DealDamages
}
