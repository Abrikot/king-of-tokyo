/**
 * Provide specific exceptions used to manage more precisely exception handling.
 */
package se.ltu.softwareengineering.exception;