/**
 * Interactions with the world. Usually useful to interact with a player.
 * <br>
 * Depend on {@link se.ltu.softwareengineering.communication}, {@link se.ltu.softwareengineering.exception}
 * and {@link se.ltu.softwareengineering.tool}
 */
package se.ltu.softwareengineering.io;